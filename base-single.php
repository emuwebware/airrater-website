<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

global $post;
$hero = Roots\Sage\Extras\get_post_thumbnail($post->ID, 'large');

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="hero" style="background-image: url('<?php echo $hero?>')">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <header>
              <a class="back btn btn-primary" href="<?php echo get_permalink( get_option('page_for_posts' ) );?>">Back to news</a>
              <h1><?php echo $post->post_title?></h1>
            </header>
          </div>
        </div>
      </div>
    </div>
    <div class="wrap container" role="document">
      <?php
      include Wrapper\template_path();
      ?>
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
