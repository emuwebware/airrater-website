<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

global $post;
$hero = Roots\Sage\Extras\get_post_thumbnail($post->ID, 'large');

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="hero" style="background-image: url('<?php echo $hero?>')">
      <h1>WHAT DOES IT MONITOR?</h1>
    </div>
    <div class="container content">
      <div class="row">
        <div class="col-sm-12">
        </div>
        <div class="col-sm-6">
          <h2>Air Pollution</h2>

          <p>Air pollution can come from many sources. One of the most important pollutants is airborne particulate matter. Particles can be produced by burning fossil fuels, wood or vegetation, or can be naturally occurring such as wind-blown soil and dust.  Bushfire and burnoffs are major sources of particulate air pollution in many parts of Australia, including the ACT, Tasmania and the Top End. This pollution can be an important health hazard for many people in the community. Smoke from wood heaters can also be a major pollution source in Tasmania and the ACT, particularly in winter. Increases in the concentration of airborne particulate matter can trigger conditions like asthma, and make other lung and heart conditions worse. In large populations, particulate air pollution is linked to increases in ambulance call outs, hospital admissions and deaths.</p>
          <p>AirRater draws its air pollution information from state and territory air quality monitoring networks. In Tasmania, information comes from EPA Tasmania’s Base Line Air Network (also known ‘BLANkET’), while in the ACT, AirRater uses data measured by the air monitoring network of ACT Health. In the NT, air quality monitoring is carried out by the NT EPA. These networks update air quality information to web based servers in near real-time. AirRater uses this data alongside statistical modelling techniques to determine likely particle concentrations across all areas of Tasmania, the ACT, and NT.</p>
          <p>The specific type of air pollution reported by AirRater is particulate matter 2.5 millionths of a metre in diameter or smaller, known as PM2.5. Particles can also be measured as PM10, meaning all particles with a diameter of up to 10 millionths of a metre. By definition, a measurement of PM10 will also include PM2.5 particles.</p>
          <p>You can see more information at the <a href="http://epa.tas.gov.au/epa/air/monitoring-air-pollution/real-time-air-quality-data-for-tasmania" target="_blank">EPA Tasmania</a>, <a href="http://www.health.act.gov.au/public-information/public-health/act-air-quality-monitoring" target="_blank">ACT Health</a> and <a href="https://ntepa.nt.gov.au/waste-pollution/air" target="_blank">NT EPA</a>.
        </div>
        <div class="col-sm-6">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/about/lorinna-jan-2016.png" alt="Bushfire at Lorinna, Tasmania">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          &nbsp;
        </div>
        <div class="col-sm-6">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/about/man-drinking-water.png" alt="Man drinking water">
        </div>
        <div class="col-sm-6">
          <h2>Temperature</h2>
          <p>Extremes of temperature, both hot and cold, can be a health hazard for many people. Extreme heat is linked to illness and death, particularly for vulnerable people such as the elderly, the very young and those with existing health conditions like diabetes and heart disease. Extremes of cold, or sudden drops in temperature, are also linked with increases in asthma.</p>
          <p>All Australian states have both cold and hot spells, and these often happen quickly and with little warning. The AirRater app uses the same methodology as the Australian Bureau of Meteorology’s Heatwave Service to determine when, where and how severe a forecast heatwave will be. Notifications will be sent to you in the heatwave affected area, giving prior warning of the conditions and allowing preparations to be made to cope with the heatwave. Similarly, a forecast extreme cold spell will be notified to you in the affected area.</p>
          <p>Research has shown that Australia is likely to see an increased number of warmer days and an increased number of heatwaves into the future. For more information on preparing for and coping with heatwaves, visit the <a href="http://www.bom.gov.au/australia/heatwave/" target="_blank">Bureau of Meteorology’s Heatwave Service website</a>, and go to the ‘Useful Links’ section for heatwave information for your region.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
        </div>
        <div class="col-sm-6">
          <h2>Pollen</h2>
          <p>Pollen is measured using a network of monitors across Tasmania and the ACT. The monitors work by capturing pollen on a sticky surface, which is then 'read' under a microscope. This involves identifying the pollen grains on the slide, and counting the number of each species. AirRater's pollen labs can identify over 25 different species, with the most common allergenic types listed in the app.</p>
          <p>We have two monitors in Tasmania (at Sandy Bay and Launceston) and one in the ACT (at the Australian National University) that allow us to collect data every day. The information from these monitors feeds directly into the AirRater app. We also have three other monitors in Tasmania (at Mornington, Devonport and Campbell Town) and one in the Northern Territory (in Darwin). At these sites, we don't have pollen counting staff, so aren't able to collect data from these monitors every day. However, we collect weekly samples, and these are sent to our Hobart lab for later analysis. We use this information to help us work towards better pollen forecasting models.</p>
          <p>Pollen is not currently available for AirRater users in the NT.</>
        </div>
        <div class="col-sm-6">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/about/boy-sneezing.png" alt="Boy sneezing">
        </div>
      </div>
      <div class="row social">
        <div class="col-md-12">
          <ul class="page-social">
            <?php $share = \Roots\Sage\Extras\get_share_links($post); ?>
            <li><a href="" class="share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
            <li><a href="<?php echo $share['facebook']?>" class="facebook" target="_blank">Share on Facebook</a></li>
            <li><a href="<?php echo $share['twitter']?>" class="twitter" target="_blank">Share on Twitter</a></li>
            <li><a href="<?php echo $share['pinterest']?>" class="pinterest" target="_blank">Share on Pinterest</a></li>
            <li><a href="<?php echo $share['linkedin']?>" class="linkedin" target="_blank">Share on LinkedIn</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
