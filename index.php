<?php

$hero = Roots\Sage\Extras\get_post_thumbnail(get_option( 'page_for_posts' ), 'large');

query_posts('posts_per_page=1');

?>
<div class="hero" style="background-image: url('<?php echo $hero?>')">
  <h1>News</h1>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2><span>Latest posts</span></h2>
      <?php while (have_posts()) : the_post(); ?>
      <article class="row post-1 post type-post status-publish format-standard hentry category-uncategorized">
        <div class="col-md-8 col-sm-6">
          <?php
          if (has_post_thumbnail()) {
            the_post_thumbnail( 'post', array( 'class' => 'img-responsive thumbnail' ) );
          } else {
            echo '<img src="http://placehold.it/600x380" class="img-responsive thumbnail">';
          }
          ?>
          <time class="updated" datetime="<?php the_time('c');?>"><span><?php the_time('d'); ?></span> <?php the_time('M Y'); ?></time>
        </div>
        <div class="col-md-4 col-sm-6">
          <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
          <div class="summary">
            <?php the_excerpt(); ?>
          </div>
          <div class="links">
            <a class="btn-more btn-outline" href="<?php the_permalink(); ?>">Read More</a>
            <ul class="page-social">
              <?php $share = Roots\Sage\Extras\get_share_links($post); ?>
              <li><a href="" class="share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
              <li><a href="<?php echo $share['facebook']?>" class="facebook" target="_blank">Share on Facebook</a></li>
              <li><a href="<?php echo $share['twitter']?>" class="twitter" target="_blank">Share on Twitter</a></li>
              <li><a href="<?php echo $share['pinterest']?>" class="pinterest" target="_blank">Share on Pinterest</a></li>
              <li><a href="<?php echo $share['linkedin']?>" class="linkedin" target="_blank">Share on LinkedIn</a></li>
            </ul>
          </div>
        </div>
        <input type="hidden" name="latest-post-id" id="latestPostId" value="<?php the_id()?>" />
      </article>
      <?php endwhile; ?>
      <h2><span>Archive</span></h2>
      <div class="row archives">
        <div class="col-md-4 post-template master">
          <article class="archive row post-1 post type-post status-publish format-standard hentry category-uncategorized">
            <div class="col-md-12 col-sm-6 col-xs-6">
              <img src="http://placehold.it/600x380" class="img-responsive thumbnail">
            </div>
            <div class="col-md-12 col-sm-6 col-xs-6">
              <h3><a href="//localhost:3000/?p=41"></a></h3>
              <time class="updated" datetime="2016-06-21T23:13:41+00:00"><span>17</span> Apr 2016</time>
              <div class="summary"></div>
              <div class="links">
                <ul class="page-social">
                  <li><a href="" class="share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
                  <li><a href="" class="facebook" target="_blank">Share on Facebook</a></li>
                  <li><a href="" class="twitter" target="_blank">Share on Twitter</a></li>
                  <li><a href="" class="pinterest" target="_blank">Share on Pinterest</a></li>
                  <li><a href="" class="linkedin" target="_blank">Share on LinkedIn</a></li>
                </ul>
              </div>
            </div>
          </article>
        </div>
      </div>
      <nav class="navigation posts-navigation" role="navigation">
        <h2 class="screen-reader-text">Posts navigation</h2>
        <div class="nav-links">
          <div class="nav-previous">
            <a href="#">Newer posts</a>
          </div>
          <div class="nav-next">
            <a href="#">Older posts</a>
          </div>
        </div>
      </nav>
      <?php
      // the_posts_navigation();
      ?>
    </div>
  </div>
</div>
