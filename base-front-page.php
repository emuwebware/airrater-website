<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

$slider_image_1 = get_field('slider_image_1');
$slider_image_2 = get_field('slider_image_2');
$slider_image_3 = get_field('slider_image_3');

$slider_data = [
  $slider_image_1['sizes']['large'],
  $slider_image_2['sizes']['large'],
  $slider_image_3['sizes']['large']
];

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
    do_action('get_header');
    get_template_part('templates/header');
    ?>
    <div class="hero-container" data-slider-images='<?php echo json_encode($slider_data)?>'>
      <div class="hero-content container">
        <div class="row">
          <div class="col-sm-8">
            <h1 class="logo">AirRater</h1>
            <p class="leader"><?php the_field('tagline'); ?></p>
            <div style="display:flex;justify-content: center;margin-bottom: 44px;">
              <div style="display: block;border:1px solid #fff;text-align: center;color: #fff;font-size: 21px;width: 371px;padding: 9px;border-radius: 9px;background: #0000006e;"><p>More information about the current smoke situation: what do the numbers mean?</p><a class="btn-more btn-outline" href="https://airrater.org/air-quality-explainer/" style="display: inline-block;">Read More</a>
              </div>
            </div>
            <ul class="actions hidden-xs">
              <li><a target="_blank" href="https://geo.itunes.apple.com/au/app/airrater/id1050535426?mt=8"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-app-store-download.png" alt="Download AirRater from the Apple App Store"/></a></li>
              <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.senset.airrater&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-play-store-download.png" alt="Download AirRater from the Google Play Store" /></a></li>
              <li><a class="yt fancybox.iframe" href="https://www.youtube.com/embed/TRLfqFv0csA"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-youtube.png" alt="Watch our video on YouTube" /></a></li>
              <!-- <li><a href="" class="btn-outline share"><i class="icon ion-android-share-alt" aria-hidden="true"></i> Share</a></li> -->
              <li><?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?></li>
            </ul>
          </div>
          <div class="col-xs-6 visible-xs-block">
            <ul class="actions">
              <li><a target="_blank" href="https://geo.itunes.apple.com/au/app/airrater/id1050535426?mt=8"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-app-store-download.png" alt="Download AirRater from the Apple App Store"/></a></li>
              <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.senset.airrater&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-play-store-download.png" alt="Download AirRater from the Google Play Store" /></a></li>
              <li><a class="yt fancybox.iframe" href="https://www.youtube.com/embed/TRLfqFv0csA"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-youtube.png" alt="Watch our video on YouTube" /></a></li>
              <li><a href="" class="btn-outline share"><i class="icon ion-android-share-alt" aria-hidden="true"></i> Share</a></li>
            </ul>
          </div>
          <div class="col-sm-4 col-xs-6">
            <img class="mockup img-responsive" src="<?= get_template_directory_uri(); ?>/dist/images/v3/phone-mockup.png" alt="AirRater app on phone">
          </div>
        </div>
      </div>
    </div>
    <a href="#whatCanItDo" class="btn-scroll"></a>
    <div class="wrap container">
      <div class="row">
        <div class="col-xs-12">
          <section id="whatCanItDo">
            <h2>What can it do for you?</h2>
            <div>
              <a class="one" href="/how-does-it-work/">
                <h3>Track your symptoms</h3>
                <p><?php the_field('wcdfy_track'); ?></p>
                <!-- <span class="btn-more btn-outline">Read More</span> -->
              </a>
              <a class="two" href="/how-does-it-work/">
                <h3>Monitor your environment</h3>
                <p><?php the_field('wcdfy_monitor'); ?></p>
                <!-- <span class="btn-more btn-outline">Read More</span> -->
              </a>
              <a class="three" href="/how-does-it-work/">
                <h3>Help manage your health</h3>
                <p><?php the_field('wcdfy_health'); ?></p>
                <!-- <span class="btn-more btn-outline">Read More</span> -->
              </a>
            </div>
          </section>
        </div>
      </div>
    </div>
    <section id="whatDoesItMonitor">
      <div class="container">
        <div class="row header">
          <div class="col-xs-12">
            <h2>What does it monitor?</h2>
          </div>
        </div>
      </div>
      <div class="smoke">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
              <h3>Smoke</h3>
              <p><?php the_field('wdim_smoke'); ?></p>
              <a class="btn-more btn-outline" href="/what-does-it-monitor">Read More</a>
              <div class="mockup">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/v3/phone-mockup-smoke.png" alt="AirRater app monitoring smoke" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="temp">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
              <h3>Temperature</h3>
              <p><?php the_field('wdim_temp'); ?></p>
              <a class="btn-more btn-outline" href="/what-does-it-monitor">Read More</a>
              <div class="mockup">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/v3/phone-mockup-temp.png" alt="AirRater app monitoring temperature" />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="pollen">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
              <h3>Pollen</h3>
              <p><?php the_field('wdim_pollen'); ?></p>
              <a class="btn-more btn-outline" href="/what-does-it-monitor">Read More</a>
              <div class="mockup">
                <img src="<?= get_template_directory_uri(); ?>/dist/images/v3/phone-mockup-pollen.png" alt="AirRater app monitoring levels of pollen" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="howDoesItWork">
      <div class="wrap container">
        <div class="row clearfix">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
            <h2>How does it work?</h2>
            <div class="row vids">
                <iframe height="450" width="800/embed/?moog_width=800" src="https://www.youtube.com/embed/TRLfqFv0csA" frameborder="0" allowfullscreen></iframe>
            </div>
            <p><?php the_field('hdiw'); ?></p>
            <a class="btn-more btn-outline" href="/how-does-it-work">Read More</a>
          </div>
        </div>
        <div class="row actions">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 clearfix">
            <a href="#" class="btn-pollen map-btn">Pollen monitors</a><a href="#" class="btn-smoke map-btn">Smoke monitors</a>
            <div class="map-container">
              <div id="area-selector"><span class="tas">Tasmania</span><span class="act">ACT</span><span class="nt">NT</span></div>
              <div id="map">
                <!-- THE MAP! -->
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
          </div>
        </div>
      </div>
    </section>
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
