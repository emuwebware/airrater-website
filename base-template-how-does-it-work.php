<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

global $post;
$hero = Roots\Sage\Extras\get_post_thumbnail($post->ID, 'large');

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="hero" style="background-image: url('<?php echo $hero?>')">
      <h1>HOW DOES IT WORK?</h1>
    </div>
    <div class="container">
      <div class="row vids">
        <div class="col-md-12">
          <iframe height="450" width="800/embed/?moog_width=800" src="https://www.youtube.com/embed/TRLfqFv0csA" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="row features">
        <div class="col-sm-6 col-md-offset-1 col-md-5">
          <h2>KEY FEATURES</h2>
          <ul>
            <li>Keep track of your hay fever and asthma symptoms in one place</li>
            <li>Check the air quality at any location</li>
            <li>Set up 'saved' locations so you can quickly view what's happening in areas you visit frequently</li>
            <li>See the location of bushfires</li>
            <li>See how your symptoms correlate with environmental conditions</li>
          </ul>
        </div>
        <div class="col-sm-6 mockup">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/v3/phone-mockup.png">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-sm-push-6">
          <p class="lead">Environmental conditions such as air quality and weather can have a major impact on our health.</p>
          <p>Environmental conditions such as poor air quality and weather can affect many people in the community by making existing illnesses, such as asthma, allergies and hay fever, worse. Severe air pollution and heat events are linked to increases in ambulance call outs, hospital admissions and death.</p>
          <p>Knowing about the quality of the air around us can help those with asthma, hay fever or other lung conditions to better manage their symptoms and improve their quality of life.</p>
          <p><strong>AirRater is a free smartphone app that can do just that.</strong></p>
          <p>AirRater was launched in October 2015 and is available for Android devices through Google Play and iOS devices (iPhone and iPad) through the App Store.</p>
        </div>
        <div class="col-sm-6 col-sm-pull-6">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/about/boy-and-puffer.jpg" alt="Asthmatic boy using a puffer">
        </div>
      </div>
      <div class="row quote">
        <div class="col-md-8 col-md-offset-2">
          <p><blockquote>A key ingredient in the success of the project is the involvement of people who have asthma, hay fever and other lung conditions.</blockquote></p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <h2>How does it work?</h2>
          <p>AirRater gathers the symptoms you’ve entered into the app and matches these with the environmental conditions at the time, building a picture of your potential environmental triggers. How quickly this occurs depends on how many symptoms you enter and whether your symptoms are seasonal or all year round.</p>
          <p>Advanced statistical modelling is used to determine this correlation, using a technique called weighted linear discriminant analysis. Each of your symptom reports is used to train the model with a suite of potential environmental triggers you were experiencing at the time, including temperature, pollen and smoke concentration. Times when you did not enter symptoms are also included in the model to provide the background environment experienced.</p>
          <p>If the model establishes a good correlation between these triggers and your symptoms, AirRater begins to send you a notification when those environmental conditions are high in your current location or any of your saved locations. This can give you time to get prepared and manage your health. The more symptom reports you enter, the better trained the statistical model will be and the more accurately it will be able to determine your triggers.</p>
          <p>When you have entered enough symptom reports, AirRater will build a picture of how your symptoms relate to the environmental conditions where you have been. AirRater features a 'Trigger Lab' that helps you explore the relationship between your symptoms and the environment. You can use this information in consultation with your health professional to work out your specific symptom triggers.</p>

          <h2>What else does it do?</h2>
          <p>Temperature and air pollution information will also be used to support community-wide air pollution health advisories, heatwave forecasting and alerts, and fire weather mapping to assist firefighters, landowners and government.</p>
          <p>For more media articles, check out AirRater in the <a href="/news">news</a>.</p>
        </div>
        <div class="col-sm-6">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/about/woman-sneezing.jpg" alt="Woman with hayfever sneezing">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">&nbsp;</div>
      </div>
      <!-- <div class="row">
        <div class="col-xs-12 col-sm-6">
          <h2>What else does it do?</h2>
          <p>Temperature and air pollution information will also be used to support community-wide air pollution health advisories, heatwave forecasting and alerts, and fire weather mapping to assist firefighters, landowners and government.</p>
          <p>For more media articles, check out AirRater in the <a href="/news">news</a>.</p>
        </div>
        <div class="col-xs-12 col-sm-6">
          <img class="img-responsive center-block" src="<?= get_template_directory_uri(); ?>/dist/images/about/man-coughing.jpg" alt="Man coughing">
        </div>
      </div> -->
      <div class="row social">
        <div class="col-md-12">
          <ul class="page-social">
            <?php $share = \Roots\Sage\Extras\get_share_links($post); ?>
            <li><a href="" class="share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
            <li><a href="<?php echo $share['facebook']?>" class="facebook" target="_blank">Share on Facebook</a></li>
            <li><a href="<?php echo $share['twitter']?>" class="twitter" target="_blank">Share on Twitter</a></li>
            <li><a href="<?php echo $share['pinterest']?>" class="pinterest" target="_blank">Share on Pinterest</a></li>
            <li><a href="<?php echo $share['linkedin']?>" class="linkedin" target="_blank">Share on LinkedIn</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
