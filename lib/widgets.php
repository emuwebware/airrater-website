<?php

namespace Roots\Sage\Widgets;


class NewsletterSignup extends \WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'newsletter_signup',
			'description' => 'Newsletter signup form',
		);
		parent::__construct( 'newsletter_signup', 'Newsletter Signup Form', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
    ?>
    <form class="newsletter-form side-bar widget" action="" method="POST">
      <?php get_template_part('templates/newsletter-form'); ?>
    </form>
    <?php
	}
}

add_action( 'widgets_init', function(){
	register_widget( __NAMESPACE__ . '\\NewsletterSignup' );
});

class ShareLinks extends \WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'ShareLinks',
			'description' => 'Share Links',
		);
		parent::__construct( 'ShareLinks', 'Share Links', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
    global $post;
    ?>
    <div class="widget">
      <ul class="page-social">
        <?php $share = \Roots\Sage\Extras\get_share_links($post); ?>
        <li><a href="" class="share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
        <li><a href="<?php echo $share['facebook']?>" class="facebook" target="_blank">Share on Facebook</a></li>
        <li><a href="<?php echo $share['twitter']?>" class="twitter" target="_blank">Share on Twitter</a></li>
        <li><a href="<?php echo $share['pinterest']?>" class="pinterest" target="_blank">Share on Pinterest</a></li>
        <li><a href="<?php echo $share['linkedin']?>" class="linkedin" target="_blank">Share on LinkedIn</a></li>
      </ul>
    </div>
    <?php
	}
}

add_action( 'widgets_init', function(){
	register_widget( __NAMESPACE__ . '\\ShareLinks' );
});

class PostDate extends \WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'PostDate',
			'description' => 'Posted Date',
		);
		parent::__construct( 'PostDate', 'Date the post/page was posted', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
    ?>
    <div class="widget">
      <div class="post-date">
        Posted <?php get_template_part('templates/entry-meta'); ?>
      </div>
    </div>
    <?php
	}
}

add_action( 'widgets_init', function(){
	register_widget( __NAMESPACE__ . '\\PostDate' );
});
