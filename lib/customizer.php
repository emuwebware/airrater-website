<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');

/**
 * Leaflet scripts and style
 */
function leaflet_scripts() {
  // if(!is_front_page()) return;
  ?>
  <link rel="stylesheet" href="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.css" />
  <script src="https://npmcdn.com/leaflet@1.0.0-rc.2/dist/leaflet.js"></script>
  <?php
}

add_action( 'wp_head', __NAMESPACE__ . '\\leaflet_scripts' );

/*
Hide editor on specific pages
*/
add_action( 'admin_init', __NAMESPACE__ . '\\hide_editor' );

function hide_editor() {
  global $pagenow;
  if( !( 'post.php' == $pagenow ) ) return;
  global $post;
  // Get the Post ID.
  $post_id = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post_ID']) ? $_POST['post_ID'] : null);
  if( !isset( $post_id ) ) return;
  // Hide the editor on the page titled 'Homepage'
  $homepgname = get_the_title($post_id);
  if($homepgname == 'Home'){
    remove_post_type_support('page', 'editor');
  }
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  // $template_file = substr( get_page_template(), strrpos( get_page_template(), '/' ) + 1 );
  // if($template_file == 'my-page-template.php'){ // the filename of the page template
  //   remove_post_type_support('page', 'editor');
  // }
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\enqueue_localized_scripts' );

function enqueue_localized_scripts() {
	wp_localize_script( 'jquery', 'ajax_global', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}

// Newsletter signup
add_action( 'wp_ajax_add_newsletter_subscriber', __NAMESPACE__ . '\\add_newsletter_subscriber' );
add_action( 'wp_ajax_nopriv_add_newsletter_subscriber', __NAMESPACE__ . '\\add_newsletter_subscriber' );

function add_newsletter_subscriber() {

  $errors = array();

  if (empty($_POST['name']))
    $errors['name'] = 'Name is required.';

  if (empty($_POST['email']))
    $errors['email'] = 'Email is required.';

  if ( ! empty($errors)) {
    $response = array(
       'success' => false,
       'errors' => $errors
    );
  }
  else {

    // $message = $_POST['name'].' has requested a newsletter subscription. Email address: '.$_POST['email'];
    $message = 'The following user has requested a newsletter subscription: '.$_POST['name'].', '.$_POST['email'];

    if(wp_mail( get_option('admin_email'), "Newsletter subscription request", $message)) {
      $response = array(
         'success' => true,
         'message' => 'Thanks! You have subscribed to our newsletter.'
      );
    } else {
      $response = array(
         'success' => false,
         'message' => 'Hmmm.. Unable to subscribe right now, please try again later.'
      );
    }

    //
    // $list_api_id = '04bd483eccead81070ae4dbffed814fa';
    // $account_api_key = 'e4949991583b3db4921449f9ae622f23';
    //
		// $cm = new \CS_REST_Subscribers( $list_api_id, $account_api_key );
    //
    // $record = array(
		// 	'EmailAddress' => $_POST['email'],
    //   'Name' => $_POST['name'],
		// 	'Resubscribe' => true
		// );
    //
		// $result = $cm->add( $record );
    //
		// if ( $result->was_successful() ) {
    //   $response = array(
    //      'success' => true
    //   );
		// }
    // else {
    //   $response = array(
    //      'success' => false,
    //      'message' => $result->response->Message
    //   );
    // }
  }

  echo json_encode($response);

  wp_die();
}

add_filter( 'query_vars', function( $vars ){
    $vars[] = 'post__in';
    $vars[] = 'post__not_in';
    return $vars;
});


add_action( 'rest_api_init', __NAMESPACE__ . '\\register_additional_post_fields' );
function register_additional_post_fields() {
    register_rest_field( 'post',
        'thumbnail_url',
        array(
            'get_callback'    => __NAMESPACE__ . '\\get_additional_post_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'date_markup',
        array(
            'get_callback'    => __NAMESPACE__ . '\\get_additional_post_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'facebook_share',
        array(
            'get_callback'    => __NAMESPACE__ . '\\get_additional_post_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'twitter_share',
        array(
            'get_callback'    => __NAMESPACE__ . '\\get_additional_post_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'pinterest_share',
        array(
            'get_callback'    => __NAMESPACE__ . '\\get_additional_post_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'linkedin_share',
        array(
            'get_callback'    => __NAMESPACE__ . '\\get_additional_post_field',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function get_additional_post_field( $object, $field_name, $request ) {
  switch($field_name) {
    case 'thumbnail_url':
      if(has_post_thumbnail($object['id'])) {
        return wp_get_attachment_image_src( get_post_thumbnail_id($object['id']), 'post')[0];
      } else {
        return "http://placehold.it/600x380";
      }
    case 'date_markup':
      $post = get_post($object['id']);
      return '<span>' . get_the_time('d', $post) . '</span> ' . get_the_time('M Y', $post);
    case 'facebook_share':
      return "http://www.facebook.com/sharer.php?u=".urlencode(get_post_permalink($object['id']));
    case 'twitter_share':
      $post = get_post($object['id']);
      return "https://twitter.com/share?url=".urlencode(get_post_permalink($object['id']))."&text=".urlencode($post->post_title)."&via=".urlencode(get_site_url());
    case 'pinterest_share':
      $post = get_post($object['id']);
      return "https://pinterest.com/pin/create/bookmarklet/?media=".urlencode(wp_get_attachment_image_src( get_post_thumbnail_id($object['id']), 'medium')[0])."&url=".urlencode(get_post_permalink($object['id']))."&is_video=false&description=".urlencode($post->post_title);
    case 'linkedin_share':
      $post = get_post($object['id']);
      return "http://www.linkedin.com/shareArticle?url=".urlencode(get_post_permalink($post))."&title=".urlencode($post->post_title);

  }
}

function addtoany_add_services( $services ) {
    $services['example_share_service'] = array(
        'name'        => 'Example Share Service',
        'icon_url'    => 'https://www.google.com/favicon.ico',
        'icon_width'  => 32,
        'icon_height' => 32,
        'href'        => 'https://www.example.com/share?url=A2A_LINKURL&title=A2A_LINKNAME',
    );
    return $services;
}
add_filter( 'A2A_SHARE_SAVE_services', __NAMESPACE__ . '\\addtoany_add_services', 10, 1 );
