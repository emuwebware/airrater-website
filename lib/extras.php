<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  // return '&hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
  return '&hellip;';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Get a post thumbnail
 */
function get_post_thumbnail($post_id, $size = 'thumbnail')
{
  return wp_get_attachment_image_src( get_post_thumbnail_id($post_id), $size)[0];
}

function get_share_links($post) {
  if(!is_object($post)) {
    $post = get_post($post);
  }
  $links = [];
  $links['facebook'] = "http://www.facebook.com/sharer.php?u=".urlencode(get_permalink($post));
  $links['twitter'] = "https://twitter.com/share?url=".urlencode(get_permalink($post))."&text=".urlencode($post->post_title)."&via=".urlencode(get_site_url());
  $links['pinterest'] = "https://pinterest.com/pin/create/bookmarklet/?media=".urlencode(wp_get_attachment_image_src( get_post_thumbnail_id($post), 'medium')[0])."&url=".urlencode(get_permalink($post))."&is_video=false&description=".urlencode($post->post_title);
  $links['linkedin'] = "http://www.linkedin.com/shareArticle?url=".urlencode(get_permalink($post))."&title=".urlencode($post->post_title);
  return $links;
}


function get_related_posts($post, $n = 3) {

  if(!is_object($post)) {
    $post = get_post($post);
  }

  $tags = wp_get_post_tags($post->ID);

  if($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $args = array(
      'tag__in' => $tag_ids,
      'post__not_in' => array($post->ID),
      'posts_per_page' => $n,
      'ignore_sticky_posts' => true
    );
  } else { // find some random posts
    $args = array(
      'posts_per_page' => $n,
      'orderby' => 'rand',
      'post__not_in' => array($post->ID)
    );
  }
  $posts = get_posts($args);
  return $posts;
}
