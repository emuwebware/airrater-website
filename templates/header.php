<header class="banner">
  <div class="container">
    <div class="nav-container">
      <a class="logo" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
      <nav class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </nav>
      <ul class="download-actions">
        <li class="app-store"><a href="https://geo.itunes.apple.com/au/app/airrater/id1050535426?mt=8" target="_blank">Download AirRater from the Apple App Store</a></li>
        <li class="play-store"><a href="https://play.google.com/store/apps/details?id=com.senset.airrater&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank">Download AirRater from the Google Play Store</a></li>
      </ul>
    </div>
    <a class="btn-menu-toggle"><i class="icon ion-android-close" aria-hidden="true"></i><i class="icon ion-navicon" aria-hidden="true"></i></a>
  </div>
</header>
