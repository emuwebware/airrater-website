<footer>
  <div id="subscribe">
    <div class="wrap container">
      <div class="row">
        <div class="col-xs-12">
          <form class="form-inline newsletter-form" action="/" method="POST">
            <?php get_template_part('templates/newsletter-form'); ?>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="sharing">
    <div class="wrap container">
      <div class="row">
        <div class="col-xs-12">
          <ul class="download-actions">
            <li><a target="_blank" href="https://geo.itunes.apple.com/au/app/airrater/id1050535426?mt=8"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-app-store-download.png" alt="Download AirRater from the Apple App Store"></a></li>
            <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.senset.airrater&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img src="<?= get_template_directory_uri(); ?>/dist/images/global/btn-play-store-download.png" alt="Download AirRater from the Google Play Store"></a></li>
          </ul>
          <ul class="social">
            <!-- <li><a href="" class="btn btn-outline share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li> -->
            <li class="universal-share"><?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?></li>
            <li><a href="https://www.facebook.com/AirRater/" class="facebook">Airrater on Facebook</a></li>
            <li><a href="https://twitter.com/AirRaterUTAS" class="twitter">Airrater on Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="partners">
    <div class="wrap container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="gov"><a href="http://www.australia.gov.au/" target="_blank">Australian Government</a></li>
            <li class="sense-t"><a href="http://www.sense-t.org.au/" target="_blank">Sense-T</a></li>
            <li class="utas-menzies"><a href="http://www.menzies.utas.edu.au/" target="_blank">Menzies University of Tasmania</a></li>
            <li class="anu"><a href="http://www.anu.edu.au/" target="_blank">Australian National University</a></li>
            <li class="csiro"><a href="http://www.csiro.au/" target="_blank">CSIRO</a></li>
            <li class="epa"><a href="http://epa.tas.gov.au/" target="_blank">Environmental Protection Agency</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="terms">
    <div class="wrap container">
      <div class="row">
        <div class="col-xs-12">
          <a href="" class="logo">AirRater</a>
          <p>Copyright &copy; AirRater <?php echo date('Y')?>. All rights reserved. <a href="/privacy-policy">Privacy Policy</a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
