
<h3>Subscribe to our newsletter</h3>
<div class="form-group">
  <label class="sr-only" for="name">Your name</label>
  <input type="text" class="form-control" name="name" id="name" placeholder="Your name" />
</div>
<div class="form-group">
  <label class="sr-only" for="email">Your email</label>
  <input type="email" class="form-control" name="email" id="email" placeholder="Your email" />
</div>
<button type="submit" class="btn btn-primary">Sign up</button>
