<?php while (have_posts()) : the_post(); ?>
  <div class="row">
    <div class="col-md-8">
      <article <?php post_class(); ?>>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>
        <?php
        // comments_template('/templates/comments.php');
        ?>
      </article>
    </div>
    <div class="col-md-4 meta">
      <?php dynamic_sidebar('sidebar-post'); ?>
    </div>
  </div>
  <?php
  $related_posts = Roots\Sage\Extras\get_related_posts($post);
  ?>

  <div class="row related-articles">
    <div class="col-md-12">
      <h2><span>Related Articles</span></h2>
      <div class="row archive">
        <?php foreach($related_posts as $related_post) { ?>
          <div class="col-md-4">
            <article class="archive row post-1 post type-post status-publish format-standard hentry category-uncategorized">
              <div class="col-md-12 col-sm-6 col-xs-6">
                <?php
                if(has_post_thumbnail($related_post->ID)) {
                  echo get_the_post_thumbnail( $related_post, 'post', array( 'class' => 'img-responsive thumbnail' ) );
                } else {
                  echo '<img src="http://placehold.it/600x380" class="img-responsive thumbnail">';
                }
                ?>
              </div>
              <div class="col-md-12 col-sm-6 col-xs-6">
                <h3><a href="<?php echo get_the_permalink($related_post); ?>" title="<?php echo get_the_title($related_post); ?>"><?php echo get_the_title($related_post); ?></a></h3>
                <time class="updated" datetime="<?php echo get_the_time('c', $related_post);?>"><span><?php echo get_the_time('d', $related_post); ?></span> <?php echo get_the_time('M Y', $related_post); ?></time>
                <div class="summary">
                  <?php echo $related_post->post_excerpt ?>
                </div>
                <div class="links">
                  <ul class="page-social">
                    <?php $share = Roots\Sage\Extras\get_share_links($related_post); ?>
                    <li><a href="" class="share"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
                    <li><a href="<?php echo $share['facebook']?>" class="facebook" target="_blank">Share on Facebook</a></li>
                    <li><a href="<?php echo $share['twitter']?>" class="twitter" target="_blank">Share on Twitter</a></li>
                    <li><a href="<?php echo $share['pinterest']?>" class="pinterest" target="_blank">Share on Pinterest</a></li>
                    <li><a href="<?php echo $share['linkedin']?>" class="linkedin" target="_blank">Share on LinkedIn</a></li>
                  </ul>
                </div>
              </div>
            </article>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
<?php endwhile; ?>
