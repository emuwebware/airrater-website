<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="/app/themes/airrater/dist/images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/app/themes/airrater/dist/images/favicon.ico" type="image/x-icon">
  <?php wp_head(); ?>
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700%7COpen+Sans:300,400" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
