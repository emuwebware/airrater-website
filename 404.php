<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2>Page not found</h2>
      <p class="lead"><?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?></p>
    </div>
  </div>
</div>
