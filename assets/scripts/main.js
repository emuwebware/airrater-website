/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

jQuery.fn.reverse = [].reverse;

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        $(".btn-menu-toggle").click(function() {
          $("header.banner").toggleClass("mobile-menu-active");
        });

        $("header.banner").has('.active.menu-item-has-children').addClass("with-sub-menu");

        var header = $('header.banner');
        var isHome = $('body').hasClass('home');
        $(window).on("scroll", function(e) {
          if(isHome) {
            if ($(this).scrollTop() > window.innerHeight) {
              header.addClass("sticky");
            } else {
              header.removeClass("sticky");
            }
          } else {
            if ($(window).scrollTop() > header.offset().top + (window.innerHeight / 2) && !(header.hasClass('sticky'))) {
              header.addClass('sticky');
            } else if ($(window).scrollTop() === 0) {
              header.removeClass('sticky');
            }
          }
        });

        $('body').scrollspy({
          target: 'nav.nav-primary',
          offset: 150
        });

        // Newsletter subscription
        var newsletterForm = $('form.newsletter-form');
        $('form.newsletter-form').submit(function(event) {

            $('.help-block', newsletterForm).remove();

            var name = $('input[name=name]', newsletterForm);
            var email = $('input[name=email]', newsletterForm);
            var formData = {
                'action': 'add_newsletter_subscriber',
                'name': name.val(),
                'email': email.val()
            };

            $.ajax({
              type        : 'POST',
              url         : "/wp/wp-admin/admin-ajax.php",
              data        : formData,
              dataType    : 'json',
              encode      : true
            })
              .done(function(response) {
                if(response.success === true) {
                  newsletterForm.addClass('subscribed');
                } else {
                  newsletterForm.addClass('failed');
                  // if (typeof response.message !== 'undefined') {
                  //   $('<div class="help-block">' + response.message + '</div>').insertAfter(email);
                  // }
                  // if (response.errors.name) {
                  //   $('<div class="help-block">' + response.errors.name + '</div>').insertAfter(name);
                  // }
                }
              });

            event.preventDefault();
        });


      },
      finalize: function() {
        $(".vids").fitVids();
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        var heroContainer = $(".home .hero-container");
        sliderImages = heroContainer.data('slider-images');
        heroContainer.vegas({
            timer: false,
            // overlay: true,
            overlay: '/app/themes/airrater/dist/images/home/overlays/10.png',
            // overlay: '/app/themes/airrater/dist/images/global/transpBlack90.png',
            // cover: true,
            slides: [
                { src: sliderImages[0] },
                { src: sliderImages[1] },
                { src: sliderImages[2] }
            ]
        });

        // Smooth scrolling
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: target.offset().top - 150
                }, 1000);
                return false;
              }
            }
        });


        var self = this;

        var map = null;
        var blanketStationMarker = L.ExtraMarkers.icon({ icon: 'ion-radio-waves', prefix: 'icon', markerColor: 'green-light', shape: 'circle'});
        var pollenStationMarker = L.ExtraMarkers.icon({ icon: 'ion-radio-waves', prefix: 'icon', markerColor: 'yellow', shape: 'circle' });
        var blanketStationsVisible = false;
        var pollenStationsVisible = false;
        var pollenStations = [];
        var blanketStations = [];

        var bounds, maxBounds, northEast, southWest;

        bounds = {
          southWest: {
            lat: -43.91372,
            lng: 143.39355
          },
          northEast: {
            lat: -39.27479,
            lng: 150.82031
          }
        };

        southWest = L.latLng(bounds.southWest.lat, bounds.southWest.lng);
        northEast = L.latLng(bounds.northEast.lat, bounds.northEast.lng);
        maxBounds = L.latLngBounds(southWest, northEast);

        var loadMap = function(mapContainerId) {
          deferred = Q.defer();

          map = L.map(mapContainerId, {
            // minZoom: 2,
            // maxZoom: 9,
            attributionControl: false,
            // maxBounds: maxBounds,
            scrollWheelZoom: false,
            doubleClickZoom: false
          });

          map.on('load', function(e) {
              deferred.resolve();
          });

          map.on('click', function(e) {
              console.log("click", e)
          });

          map.on('drag', function(e) {
              console.log(map.getCenter());
          });

          map.on('zoom', function(e) {
              console.log("zoom", e)
          });

          // map.setView([-42.160009, 146.696650], 4);
          map.setView([-27.49578587297129, 135.1689147949219], 4);
          // map.setView([-37.107765071185135, 147.3486328125], 6);

          L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
              attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
              // maxZoom: 4,
              id: 'clucani.n3a3ieb7',
              accessToken: 'pk.eyJ1IjoiY2x1Y2FuaSIsImEiOiI5OTVmMjQ0MWI4N2ZhZjgwZDMyYmMwMjE4YzE5MDUxZiJ9.cRADywo7DzGVlokdzLHL9w'
          }).addTo(map);

          return deferred.promise;
        };


        var buildPollenStations = function(data) {
          var stations = [];
          for(var i=0; i<data.Name.length; i++) {
            stations.push({
              code: data.Code[i],
              lat: data.Lat[i],
              lng: data.Lon[i],
              name: data.Name[i],
              schedule: data.Schedule[i]
            });
          }
          return stations;
        };

        var buildBlanketStations = function(data) {
          var stations = [];
          for(var i=0; i<data.Name.length; i++) {
            stations.push({
              name: data.Name[i],
              code: data.Code[i],
              elevation: data.Ele[i],
              lat: data.Lat[i],
              lng: data.Lon[i]
            });
          }
          return stations;
        };

        var toggleStations = function(stations) {
          for(var i=0; i < stations.length; i++) {
            if(map.hasLayer(stations[i])) {
              map.removeLayer(stations[i]);
            } else {
              stations[i].addTo(map);
            }
          }
        };

        var addPollenStation = function(station) {
          var latlng = L.latLng(station.lat, station.lng);
          var marker = new L.marker(latlng, {icon: pollenStationMarker});
          pollenStations.push(marker);
        };

        var addBlanketStation = function(station) {
          var latlng = L.latLng(station.lat, station.lng);
          var marker = new L.marker(latlng, {icon: blanketStationMarker});
          blanketStations.push(marker);
        };

        var getPollenStations = function() {
          var deferred = Q.defer();
          $.get( "https://r.airrater.org/custom/q/", { q: '{"Type":"PollenStationList"}' }).done(function( data ) {
            deferred.resolve(buildPollenStations(data));
          });
          return deferred.promise;
        };

        var getBlanketStations = function() {
          var deferred = Q.defer();
          $.get( "https://r.airrater.org/custom/q/", { q: '{"Type":"StationList"}' }).done(function( data ) {
            deferred.resolve(buildBlanketStations(data));
          });
          return deferred.promise;
        };


        loadMap('map').then(function() {
          getPollenStations().then(function(stations) {
            for(var i=0; i < stations.length; i++) {
              addPollenStation(stations[i]);
            }
            toggleStations(pollenStations);
          });
          getBlanketStations().then(function(stations) {
            for(var i=0; i < stations.length; i++) {
              addBlanketStation(stations[i]);
            }
            toggleStations(blanketStations);
          });
        });

        $('a.btn-pollen').on('click', function(e) {
          e.preventDefault();
          toggleStations(pollenStations);
        });

        $('a.btn-smoke').on('click', function(e) {
          e.preventDefault();
          toggleStations(blanketStations);
        });

        $('#area-selector .tas').on('click', function(e) {
          if(map) {
            map.flyTo([-42.09764923385693, 146.74597263336184], 7);
          }
          // map.setView([-37.107765071185135, 147.3486328125], 6);
        });

        $('#area-selector .act').on('click', function(e) {
          if(map) {
            map.flyTo([-35.305636869356015, 149.11032110452655], 11);
          }
        });

        $('#area-selector .nt').on('click', function(e) {
          if(map) {
            map.flyTo([-12.462563644288316, 130.9156742691994], 10);
          }
        });

      	$(".yt").fancybox({
      		maxWidth	: 800,
      		maxHeight	: 600,
      		fitToView	: false,
      		width		: '70%',
      		height		: '70%',
      		autoSize	: false,
      		closeClick	: false,
      		openEffect	: 'none',
      		closeEffect	: 'none'
      	});



      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // About us page, note the change from about-us to about_us.
    'blog': {
      init: function() {

        var postsPerPage = 3;
        var currentPage = 1;
        var totalPages = 0;
        var hasNext = false;
        var hasPrev = false;

        var btnNext = $('.nav-next a');
        var btnPrev = $('.nav-previous a');

        var posts = [];
        var postsContainer = $('.archives');
        var postTemplate = $('.post-template.master', postsContainer);
        var latestPostId = $('#latestPostId');

        var removeCurrentPosts = function() {
          q = Q.defer();
          postsContainer.css("height", postsContainer.height());
          $('> div:not(.master)', postsContainer).remove();
          q.resolve();
          return q.promise;
        };

        var addPost = function(data) {
          var post = postTemplate.clone();
          $('h3 a', post).text(data.title.rendered).attr("href", data.link);
          $('.summary', post).html(data.excerpt.rendered);
          $('img.thumbnail', post).attr("src", data.thumbnail_url);
          $('time.updated', post).html(data.date_markup).attr("datetime", data.date);
          $('a.facebook', post).attr("href", data.facebook_share);
          $('a.twitter', post).attr("href", data.twitter_share);
          $('a.pinterest', post).attr("href", data.pinterest_share);
          $('a.linkedin', post).attr("href", data.linkedin_share);
          post.appendTo(postsContainer).removeClass('master post-template');
        };

        var updatePaging = function() {
          btnPrev.toggle(hasPrev);
          btnNext.toggle(hasNext);
        };

        var getPosts = function() {
          postsContainer.addClass("loading");
          $.ajax( {
            url: '/wp-json/wp/v2/posts?per_page=' + postsPerPage + '&page=' + currentPage + "&filter[post__not_in][]=" + latestPostId.val(),
            success: function (data, txtStatus, jqXHR) {

              totalPages = jqXHR.getResponseHeader('x-wp-totalpages');
              hasPrev = currentPage > 1;
              hasNext = currentPage < totalPages;

              updatePaging();

              removeCurrentPosts().then(function(){
                $(data).each(function(index) {
                  addPost(data[index]);
                });
                postsContainer.css("height", "auto");
              });
              postsContainer.removeClass("loading");
            },
            cache: false
          } );
        };

        btnNext.on('click', function(e) {
          e.preventDefault();
          if(!hasNext){ return; }
          currentPage++;
          getPosts();
        });

        btnPrev.on('click', function(e) {
          e.preventDefault();
          if(!hasPrev){ return; }
          currentPage--;
          getPosts();
        });

        updatePaging();
        getPosts();

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
